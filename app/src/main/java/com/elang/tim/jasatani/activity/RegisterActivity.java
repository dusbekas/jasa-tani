package com.elang.tim.jasatani.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.widget.ProgressDialogHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {

    String TAG = RegisterActivity.class.getSimpleName();

    private FirebaseAuth auth;
    private EditText edtEmail, edtPassword;
    private Button btnSignUp;
    private TextView txtLogOn;
    private RadioGroup rdgType;
    private RadioButton rdbAhli, rdbPemilik;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnSignUp = findViewById(R.id.btn_sign_up);
        txtLogOn = findViewById(R.id.txt_log_on);
        rdgType = findViewById(R.id.rdgType);
        rdbAhli = findViewById(R.id.rdbAhli);
        rdbPemilik = findViewById(R.id.rdbPemilik);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialogHelper progressDialogHelper = new ProgressDialogHelper();
                if (!validateForm()) {
                    return;
                }
                progressDialogHelper.showDialog(progressDialog);

                // Get data from layout
                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();
                String accountType ="";
                if (rdbAhli.isChecked()) {
                    accountType = "ahli";
                } else if (rdbAhli.isChecked()) {
                    accountType = "pemilik";
                }

                // Create new account
                final String finalAccountType = accountType;
                auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(
                        RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Register is success, do something.
                                    FirebaseUser user = auth.getCurrentUser();
                                    Log.d(TAG, "createUserWithEmail:success " + user);
                                    String email = user.getEmail();
                                    String uid = user.getUid();
                                    showDataCompletion(email, uid, finalAccountType);
                                    progressDialogHelper.hideDialog(progressDialog);
                                } else {
                                    // If failed, display message.
                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                    progressDialogHelper.hideDialog(progressDialog);
                                    Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                    // TODO show toast message based on error
                                }
                            }
                        });
            }
        });

        txtLogOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LogOnActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null){
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void showDataCompletion(String email, String uid, String accountType){
        Intent intent = new Intent(RegisterActivity.this, DataCompletionActivity.class);
        intent.putExtra("EMAIL", email);
        intent.putExtra("UID", uid);
        intent.putExtra("ACCOUNTTYPE", accountType);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        startActivity(intent);
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = edtEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            edtEmail.setError("Required.");
            valid = false;
        } else {
            edtEmail.setError(null);
        }

        String password = edtPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            edtPassword.setError("Required.");
            valid = false;
        } else {
            edtPassword.setError(null);
        }

        if (rdgType.getCheckedRadioButtonId()==-1) {
            rdbAhli.setError("Required.");
            rdbPemilik.setError("Required.");
        } else {
            rdbAhli.setError(null);
            rdbPemilik.setError(null);
        }

        return valid;
    }
}
