package com.elang.tim.jasatani.widget;

import android.app.ProgressDialog;

/**
 * Created by arman on 10/08/2017.
 */

public class ProgressDialogHelper {

    public void showDialog(ProgressDialog p) {
        if (!p.isShowing())
            p.show();
    }

    public void hideDialog(ProgressDialog p) {
        if (p.isShowing())
            p.dismiss();
    }
}
