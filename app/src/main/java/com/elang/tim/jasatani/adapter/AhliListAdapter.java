package com.elang.tim.jasatani.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.activity.DetailAhliActivity;
import com.elang.tim.jasatani.model.Ahli;

import java.util.List;

/**
 * Created by Arman Muhamad on 18/12/2017.
 */

public class AhliListAdapter extends RecyclerView.Adapter<AhliListAdapter.ViewHolder> {

    private List<Ahli> ahliList;
    private Context context;

    public AhliListAdapter(List<Ahli> ahliList, Context context){
        this.ahliList = ahliList;
        this.context = context;
    }

    @Override
    public AhliListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_browse_ahli, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Ahli ahli = ahliList.get(position);

        String name = ahli.getName();
        /*float rating = ahli.getRating();
        String feedback = ahli.getFeedback()+ " feedback";*/

        holder.name.setText(name);
        /*holder.feedback.setText(feedback);
        holder.rating.setRating(rating);*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetailAhliActivity.class);
                intent.putExtra("ID", ahliList.get(position).getId());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ahliList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private RatingBar rating;
        private TextView feedback;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txt_browse_ahli_nama);
            rating = itemView.findViewById(R.id.rat_browse_ahli);
            feedback = itemView.findViewById(R.id.txt_browse_feedback);
        }
    }
}
