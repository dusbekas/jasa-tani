package com.elang.tim.jasatani.activity;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.adapter.LahanListAdapter;
import com.elang.tim.jasatani.helper.UserPreferences;
import com.elang.tim.jasatani.model.Lahan;
import com.elang.tim.jasatani.presenter.LahanContract;
import com.elang.tim.jasatani.presenter.LahanPresenter;

import java.util.List;

public class LahanDialogueActivity extends AppCompatActivity implements LahanContract.View {

    private LahanContract.Presenter presenter;

    private UserPreferences preferences;
    private ProgressBar pgbLahanList;
    private RecyclerView revLahanList;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lahan_dialogue);

        pgbLahanList = findViewById(R.id.pgb_lahan_list);
        revLahanList = findViewById(R.id.rev_lahan_list);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_my_lahan);

        // Initialisation
        preferences = new UserPreferences(getApplicationContext());
        presenter = new LahanPresenter(this);

        // Configuring recyclerview
        revLahanList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        revLahanList.setLayoutManager(layoutManager);
        DividerItemDecoration decoration = new DividerItemDecoration(
                revLahanList.getContext(),
                DividerItemDecoration.VERTICAL);
        revLahanList.addItemDecoration(decoration);

        // Getting and populating data for list
        presenter.getAvailableLahanList(preferences.getId());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showAvailableLahanList(List<Lahan> lahanList) {
        revLahanList.setVisibility(View.VISIBLE);
        pgbLahanList.setVisibility(View.GONE);
        adapter = new LahanListAdapter(lahanList, getApplicationContext(), this, this);
        revLahanList.setAdapter(adapter);
    }
}
