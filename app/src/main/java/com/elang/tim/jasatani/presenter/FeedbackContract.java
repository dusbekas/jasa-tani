package com.elang.tim.jasatani.presenter;

/**
 * Created by arman on 02/01/2018.
 */

public interface FeedbackContract {

    interface Presenter {

        void calculate();
        void giveFeedback();
    }

}
