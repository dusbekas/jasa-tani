package com.elang.tim.jasatani.presenter;

import android.content.Context;

import com.elang.tim.jasatani.model.Lahan;

import java.util.List;

/**
 * Created by arman on 02/01/2018.
 */

public interface LahanContract {

    interface Presenter {

        void addLahan();
        void changeDescription();
        void getAvailableLahanList(int idPemilik);
        void onRecyclerClicked(String activitySource, Context context, int id);

    }

    interface View {

        void showAvailableLahanList(List<Lahan> lahanList);

    }
}
