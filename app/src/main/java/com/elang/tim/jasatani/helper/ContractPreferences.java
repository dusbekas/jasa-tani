package com.elang.tim.jasatani.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by arman on 07/01/2018.
 */

public class ContractPreferences {

    private static String TAG = ContractPreferences.class.getName();

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    int MODE = 0;
    private static final String PREF_NAME = "ContractPreferences";

    private String ID_AHLI = "id_ahli";
    private String ID_LAHAN = "id_lahan";
    private String CONTRACT_DATE = "contract_date";
    private String CONTRACT_DURATION = "contract_duration";
    private String CONTRACT_STATUS = "contract_status";

    public ContractPreferences(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREF_NAME, MODE);
        editor = preferences.edit();
    }

    public void setIdAhli(int data) {
        editor.putInt(ID_AHLI, data);
        editor.commit();
        Log.d(TAG, ID_AHLI + "has been modified.");
    }

    public void setIdLahan(int data) {
        editor.putInt(ID_LAHAN, data);
        editor.commit();
        Log.d(TAG, ID_LAHAN + "has been modified.");
    }

    public void setContractDate(String data) {
        editor.putString(CONTRACT_DATE, data);
        editor.commit();
        Log.d(TAG, CONTRACT_DATE + "has been modified.");
    }

    public void setContractDuration(int data) {
        editor.putInt(CONTRACT_DURATION, data);
        editor.commit();
        Log.d(TAG, CONTRACT_DURATION + "has been modified.");
    }

    public void setContractStatus(String data) {
        editor.putString(CONTRACT_STATUS, data);
        editor.commit();
        Log.d(TAG, CONTRACT_STATUS + "has been modified.");
    }

    public int getIdAhli() {
        return preferences.getInt(ID_AHLI, -1);
    }

    public int getIdLahan() {
        return preferences.getInt(ID_LAHAN, -1);
    }

    public String setContractDate() {
        return preferences.getString(CONTRACT_DATE, "");
    }

    public int setContractDuration() {
        return preferences.getInt(CONTRACT_DURATION, -1);
    }

    public String setContractStatus() {
        return preferences.getString(CONTRACT_STATUS, "");
    }

    public void clearContractPreferences() {
        editor.clear();
        editor.apply();
    }

}