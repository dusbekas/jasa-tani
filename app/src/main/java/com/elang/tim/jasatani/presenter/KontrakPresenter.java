package com.elang.tim.jasatani.presenter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.activity.ContractActivity;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.HandleVolleyError;
import com.elang.tim.jasatani.model.Kontrak;
import com.elang.tim.jasatani.model.Lahan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by arman on 09/01/2018.
 */

public class KontrakPresenter implements KontrakContract.Presenter {

    private static String TAG = KontrakPresenter.class.getName();

    private KontrakContract.View view;

    private List<Kontrak> kontrakList;

    public KontrakPresenter(KontrakContract.View view) {
        this.view = view;
    }

    @Override
    public void createContract(final int idAhli, final int idLahan, final String date,
                               final String duration) {
        StringRequest request = new StringRequest(Request.Method.POST,
                AppConfig.URL_KONTRAK_AHLI_TANI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, response);
                            JSONObject object = new JSONObject(response);
                            Boolean condition = object.getBoolean("error");
                            if (!condition) {
                                // do something
                                view.finishSendContract("Contract has been sent.");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, e.toString());
                            view.finishSendContract(String.valueOf(e));
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError volleyError = new HandleVolleyError();
                Log.d(TAG, volleyError.handleVolleyError(error));
                view.finishSendContract(volleyError.handleVolleyError(error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                Log.d(TAG, idAhli + " " + idLahan + " " + date + " " + duration);
                map.put("id_ahli_tani", String.valueOf(idAhli));
                map.put("id_lahan", String.valueOf(idLahan));
                map.put("tanggal_kontrak", date);
                map.put("durasi_kontrak", duration);

                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    // TODO: Change URL
    @Override
    public void getKontrakList(final int idPemilik, final String status) {
        StringRequest request = new StringRequest(Request.Method.POST,
                AppConfig.URL_LIST_LAHAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            kontrakList = new ArrayList<>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                Kontrak kontrak = new Kontrak();
                                JSONObject object;
                                try {
                                    object = jsonArray.getJSONObject(i);
                                    Log.d(TAG, object.toString());
                                    kontrak.setIdAhli(Integer.valueOf(object.getString("id_ahli_tani")));
                                    kontrak.setIdLahan(Integer.valueOf(object.getString("id_lahan")));
                                    kontrak.setDate(object.getString("tanggal_kontrak"));
                                    kontrak.setDuration(Integer.valueOf(object.getString("durasi_kontrak")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                kontrakList.add(kontrak);
                            }
                            view.showKontrakList(kontrakList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError volleyError = new HandleVolleyError();
                //volleyError.handleVolleyError(error);
                Log.d(TAG, volleyError.handleVolleyError(error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id_pemilik", String.valueOf(idPemilik));
                map.put("status", status);

                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void createDate(int year, int monthOfYear, int dayOfMonth) {
        String dd, mm, yyyy;
        dd = String.valueOf(dayOfMonth);
        mm = String.valueOf(monthOfYear+1);
        yyyy = String.valueOf(year);
        if (dayOfMonth<10) {
            dd = "0"+dd;
        }
        if (monthOfYear<10) {
            mm = "0"+mm;
        }
        view.updateDate(dd + "-" + mm + "-" + yyyy);
    }

    @Override
    public void createLahan(String lahan) {
        view.updateLahan(lahan);
    }
}
