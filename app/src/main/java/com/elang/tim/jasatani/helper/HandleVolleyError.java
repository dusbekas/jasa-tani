package com.elang.tim.jasatani.helper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

/**
 * Created by arman on 27/09/2017.
 */

public class HandleVolleyError {
    public String handleVolleyError (VolleyError volleyError) {
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet, please check your connection.";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found, please try again after some time.";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet, please check your connection.";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error, please try again after some time.";
        }  else if (volleyError instanceof TimeoutError) {
            message = "Connection timeout, please check your connection.";
        }
        return message;
    }
}
