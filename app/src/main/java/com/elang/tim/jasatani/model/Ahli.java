package com.elang.tim.jasatani.model;

/**
 * Created by Arman Muhamad on 18/12/2017.
 */

public class Ahli {
    private int id;
    private String email;
    private String name;
    private String address;
    private String phone;
    private String description;
    private String team;
    private float rating;
    private int feedback;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public float getRating() {
        return rating;
    }

    public int getFeedback() {
        return feedback;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setFeedback(int feedback) {
        this.feedback = feedback;
    }
}
