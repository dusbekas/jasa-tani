package com.elang.tim.jasatani.presenter;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.ContractPreferences;
import com.elang.tim.jasatani.helper.HandleVolleyError;
import com.elang.tim.jasatani.helper.UserPreferences;
import com.elang.tim.jasatani.model.Lahan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by arman on 04/01/2018.
 */

public class LahanPresenter implements LahanContract.Presenter {

    private static String TAG = LahanPresenter.class.getName();

    private LahanContract.View view;

    private List<Lahan> lahanList;

    public LahanPresenter(LahanContract.View view) {
        this.view = view;
    }

    @Override
    public void addLahan() {

    }

    @Override
    public void changeDescription() {

    }

    @Override
    public void getAvailableLahanList(final int idPemilik) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                AppConfig.URL_LIST_LAHAN_PEMILIK_AVAILABLE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            lahanList = new ArrayList<>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                Lahan lahan = new Lahan();
                                JSONObject object;
                                try {
                                    object = jsonArray.getJSONObject(i);
                                    Log.d(TAG, object.toString());
                                    lahan.setArea(Integer.valueOf(object.getString("luas_lahan")));
                                    lahan.setDescription(object.getString("deskripsi_lahan"));
                                    lahan.setId(Integer.valueOf(object.getString("id_lahan")));
                                    lahan.setLocation(object.getString("lokasi_lahan"));
                                    lahan.setStatus(object.getString("status"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                lahanList.add(lahan);
                            }
                            view.showAvailableLahanList(lahanList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError volleyError = new HandleVolleyError();
                //volleyError.handleVolleyError(error);
                Log.d(TAG, volleyError.handleVolleyError(error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                map.put("id_pemilik_lahan", String.valueOf(idPemilik));

                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onRecyclerClicked(String activitySource, Context context, int id) {
        ContractPreferences preferences = new ContractPreferences(context);
        preferences.setIdLahan(id);
    }
}
