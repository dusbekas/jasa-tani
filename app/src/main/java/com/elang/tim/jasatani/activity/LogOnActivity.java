package com.elang.tim.jasatani.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.HandleVolleyError;
import com.elang.tim.jasatani.helper.UserPreferences;
import com.elang.tim.jasatani.widget.ProgressDialogHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogOnActivity extends AppCompatActivity {

    String TAG = RegisterActivity.class.getSimpleName();

    private FirebaseAuth auth;

    private String email, password;

    private Button btnLogOn;
    private EditText edtEmail, edtPassword;
    private ProgressDialog progressDialog;

    private UserPreferences taniPreferences;
    private ProgressDialogHelper progressDialogHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_on);

        taniPreferences = new UserPreferences(getApplicationContext());
        progressDialogHelper = new ProgressDialogHelper();

        auth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        edtEmail = (EditText)findViewById(R.id.edt_email);
        edtPassword = (EditText)findViewById(R.id.edt_password);
        btnLogOn = (Button)findViewById(R.id.btn_log_on);

        btnLogOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateForm()) {
                    return;
                }
                progressDialogHelper.showDialog(progressDialog);

                // Get data from layout
                email = edtEmail.getText().toString();
                password = edtPassword.getText().toString();

                // check account
                auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                        LogOnActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Log on is success, do something
                                    FirebaseUser user = auth.getCurrentUser();
                                    Log.d(TAG, "logOnWithEmail:success " + user);

                                    logonUser();
                                } else {
                                    // If failed, display message.
                                    Log.w(TAG, "logOnWithEmail:failure", task.getException());
                                    Toast.makeText(LogOnActivity.this, "Authenticaation failed,",
                                            Toast.LENGTH_SHORT).show();
                                    progressDialogHelper.hideDialog(progressDialog);
                                    // TODO show toast message based on error
                                }
                            }
                        }
                );
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null){
            Intent intent  = new Intent(LogOnActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = edtEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            edtEmail.setError("Required.");
            valid = false;
        } else {
            edtEmail.setError(null);
        }

        String password = edtPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            edtPassword.setError("Required.");
            valid = false;
        } else {
            edtPassword.setError(null);
        }

        return valid;
    }

    private void logonUser() {
        StringRequest request = new StringRequest(Request.Method.POST, AppConfig.URL_LOGON,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Logon Response: " + response);

                        try {
                            JSONObject object = new JSONObject(response);
                            String errorValue = object.getString("error");
                            if (errorValue.equals("true")){
                                Toast.makeText(LogOnActivity.this,
                                        object.getString("error_msg"),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                JSONObject user = object.getJSONObject("user");
                                int id = user.getInt("id");
                                String name = user.getString("name");
                                String email = user.getString("email");
                                String phone = user.getString("phone");
                                String address = user.getString("address");
                                String uid = user.getString("uid");
                                String type = user.getString("type");
                                taniPreferences.setId(id);
                                taniPreferences.setName(name);
                                taniPreferences.setEmail(email);
                                taniPreferences.setPhone(phone);
                                taniPreferences.setAddress(address);
                                taniPreferences.setAccType(type);
                                taniPreferences.setUid(uid);

                                Toast.makeText(LogOnActivity.this, "Logon success",
                                        Toast.LENGTH_SHORT).show();
                                Intent intent  = new Intent(
                                        LogOnActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialogHelper.hideDialog(progressDialog);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError handleVolleyError = new HandleVolleyError();
                Toast.makeText(
                        LogOnActivity.this,
                        handleVolleyError.handleVolleyError(error),
                        Toast.LENGTH_LONG).show();

                progressDialogHelper.hideDialog(progressDialog);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", auth.getCurrentUser().getEmail());
                params.put("uid", auth.getCurrentUser().getUid());

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }
}
