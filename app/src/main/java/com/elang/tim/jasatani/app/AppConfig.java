package com.elang.tim.jasatani.app;

/**
 * Created by arman on 03/07/2017.
 */

public class AppConfig {
    // Local development
    public static String SERVER = "http://192.168.1.122/jasa-tani-backend/";

    // Public server
    //public static String SERVER = "http://tim-elang.000webhostapp.com/";

    public static String URL_REGISTER_AHLI = SERVER+"register_ahli.php";
    public static String URL_REGISTER_PEMILIK = SERVER+"register_pemilik.php";
    public static String URL_LOGON = SERVER+"logon.php";
    public static String URL_LIST_AHLI = SERVER+"list_ahli.php";
    public static String URL_DETAIL_AHLI = SERVER+"detail_ahli_tani.php";
    public static String URL_LIST_LAHAN = SERVER+"list_lahan.php";
    public static String URL_LIST_LAHAN_PEMILIK_AVAILABLE =
            SERVER+"list_lahan_pemilik_tersedia.php";
    public static String URL_KONTRAK_AHLI_TANI = SERVER+"kontrak_ahli_tani.php";

}
