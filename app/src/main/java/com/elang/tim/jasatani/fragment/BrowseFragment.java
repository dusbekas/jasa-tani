package com.elang.tim.jasatani.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.activity.DetailAhliActivity;
import com.elang.tim.jasatani.adapter.AhliListAdapter;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.HandleVolleyError;
import com.elang.tim.jasatani.model.Ahli;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class BrowseFragment extends Fragment {

    private String TAG = BrowseFragment.class.getName();

    private ProgressBar pgbAhliList;
    private RecyclerView revAhliList;

    private List<Ahli> ahliList;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    public static BrowseFragment newInstance() {
        BrowseFragment fragment = new BrowseFragment();
        return fragment;
    }


    public BrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_browse, container, false);

        pgbAhliList = view.findViewById(R.id.pgb_ahli_list);
        revAhliList = view.findViewById(R.id.rev_ahli_list);

        // Configure recyclerview
        ahliList = new ArrayList<>();
        revAhliList = view.findViewById(R.id.rev_ahli_list);
        revAhliList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        revAhliList.setLayoutManager(layoutManager);

        // Get data
        getData();

        return view;
    }

    private void getData(){
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, AppConfig.URL_LIST_AHLI,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i=0; i<response.length(); i++) {
                    Ahli ahli = new Ahli();
                    JSONObject object;
                    try {
                        object = response.getJSONObject(i);
                        Log.d(TAG, object.toString());
                        ahli.setId(Integer.valueOf(object.getString("id_ahli_tani")));
                        ahli.setEmail(object.getString("email_ahli_tani"));
                        ahli.setName(object.getString("nama_ahli_tani"));
                        ahli.setAddress(object.getString("alamat_ahli_tani"));
                        ahli.setPhone(object.getString("no_telpon_ahli_tani"));
                        // TODO: Then get rating and feedback count
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ahliList.add(ahli);
                }
                revAhliList.setVisibility(View.VISIBLE);
                pgbAhliList.setVisibility(View.GONE);
                adapter = new AhliListAdapter(ahliList, getContext());
                revAhliList.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError volleyError = new HandleVolleyError();
                volleyError.handleVolleyError(error);
                revAhliList.setVisibility(View.VISIBLE);
                pgbAhliList.setVisibility(View.GONE);
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

}
