package com.elang.tim.jasatani.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.helper.ContractPreferences;
import com.elang.tim.jasatani.model.Kontrak;
import com.elang.tim.jasatani.presenter.KontrakContract;
import com.elang.tim.jasatani.presenter.KontrakPresenter;

import java.util.List;

public class ContractActivity extends AppCompatActivity implements KontrakContract.View {

    private KontrakContract.Presenter presenter;

    private static int LAHAN_CODE = 1;
    private static int DATE_CODE = 2;

    private Button btnSend;
    private EditText edtLahan, edtDate, edtDuration;
    private Toolbar toolbar;
    private DatePickerDialog pickerDialog;

    private ContractPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract);

        btnSend = findViewById(R.id.btn_send_contract);
        edtLahan = findViewById(R.id.edt_lahan);
        edtDate = findViewById(R.id.edt_date);
        edtDuration = findViewById(R.id.edt_duration);
        toolbar = findViewById(R.id.toolbar);

        presenter = new KontrakPresenter(this);
        preferences = new ContractPreferences(getApplicationContext());

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_create_contract);

        edtLahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContractActivity.this, LahanDialogueActivity.class);
                startActivityForResult(intent, LAHAN_CODE);
            }
        });

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                java.util.Calendar calendar = java.util.Calendar.getInstance();
                pickerDialog = new DatePickerDialog(ContractActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                presenter.createDate(year, monthOfYear, dayOfMonth);
                            }
                        }, calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DAY_OF_MONTH));
                pickerDialog.show();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.createContract(preferences.getIdAhli(), preferences.getIdLahan(),
                        edtDate.getText().toString(), edtDuration.getText().toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    presenter.createLahan(data.getStringExtra("LAHAN"));
                }
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void updateLahan(String lahan) {
        edtLahan.setText(lahan);
    }

    @Override
    public void updateDate(String date) {
        edtDate.setText(date);
    }

    @Override
    public void finishSendContract(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void showKontrakList(List<Kontrak> kontrakList) {

    }
}
