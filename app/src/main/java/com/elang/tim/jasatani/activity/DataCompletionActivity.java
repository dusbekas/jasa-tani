package com.elang.tim.jasatani.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.UserPreferences;
import com.elang.tim.jasatani.helper.HandleVolleyError;
import com.elang.tim.jasatani.widget.ProgressDialogHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DataCompletionActivity extends AppCompatActivity {

    private String TAG = DataCompletionActivity.class.getSimpleName();

    private String email, uid, accountType;

    private EditText edtName, edtAddress, edtPhone;
    private Button btnSave;
    private ProgressDialog progressDialog;

    private UserPreferences taniPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_completion);
        taniPreferences = new UserPreferences(getApplicationContext());

        if (savedInstanceState==null) {
            Bundle extras = getIntent().getExtras();
            if(extras==null) {
                email = null;
                uid = null;
                accountType=null;
            } else {
                email = extras.getString("EMAIL");
                uid = extras.getString("UID");
                accountType = extras.getString("ACCOUNTTYPE");
            }
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        edtName = findViewById(R.id.edt_data_completion_name);
        edtAddress = findViewById(R.id.edt_data_completion_address);
        edtPhone = findViewById(R.id.edt_data_completion_phone);
        btnSave = findViewById(R.id.btn_data_completion_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateForm()) {
                    return;
                }
                registerUser(email, edtName.getText().toString(), edtPhone.getText().toString(),
                        edtAddress.getText().toString(), uid, accountType);
            }
        });

    }

    public void registerUser(final String email, final String name, final String phone,
                             final String address, final String uid, final String accountType){

        String Uri;
        final ProgressDialogHelper progressDialogHelper = new ProgressDialogHelper();
        progressDialogHelper.showDialog(progressDialog);

        if (accountType.equals("ahli")) {
            Uri = AppConfig.URL_REGISTER_AHLI;
        } else {
            Uri = AppConfig.URL_REGISTER_PEMILIK;
        }

        StringRequest request = new StringRequest(
                Request.Method.POST, Uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response);

                try {
                    JSONObject object = new JSONObject(response);
                    String errorValue = object.getString("error");
                    if (errorValue.equals("true")){
                        Toast.makeText(DataCompletionActivity.this,
                                object.getString("error_msg"),
                                Toast.LENGTH_LONG).show();
                    } else {
                        JSONObject user = object.getJSONObject("user");
                        int id = user.getInt("id");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String phone = user.getString("phone");
                        String address = user.getString("address");
                        String uid = user.getString("uid");
                        String type = user.getString("type");
                        taniPreferences.setId(id);
                        taniPreferences.setName(name);
                        taniPreferences.setEmail(email);
                        taniPreferences.setPhone(phone);
                        taniPreferences.setAddress(address);
                        taniPreferences.setAccType(type);
                        taniPreferences.setUid(uid);
                        Intent intent  = new Intent(
                                DataCompletionActivity.this, RegisterActivity.class);
                        /*intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_NEW_TASK);*/
                        startActivity(intent);
                        /*finish();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialogHelper.hideDialog(progressDialog);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError handleVolleyError = new HandleVolleyError();
                Toast.makeText(
                        DataCompletionActivity.this,
                        handleVolleyError.handleVolleyError(error),
                        Toast.LENGTH_LONG).show();

                progressDialogHelper.hideDialog(progressDialog);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", email);
                params.put("name", name);
                params.put("phone", phone);
                params.put("address", address);
                params.put("uid", uid);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    private boolean validateForm() {
        boolean valid = true;

        String name = edtName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            edtName.setError("Required.");
            valid = false;
        } else {
            edtName.setError(null);
        }

        String address = edtAddress.getText().toString();
        if (TextUtils.isEmpty(address)) {
            edtAddress.setError("Required.");
            valid = false;
        } else {
            edtAddress.setError(null);
        }

        String phone = edtPhone.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            edtPhone.setError("Required.");
            valid = false;
        } else {
            edtPhone.setError(null);
        }

        return valid;
    }
}
