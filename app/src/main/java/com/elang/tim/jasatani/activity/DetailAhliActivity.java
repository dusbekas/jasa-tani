package com.elang.tim.jasatani.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.app.AppConfig;
import com.elang.tim.jasatani.app.AppController;
import com.elang.tim.jasatani.helper.ContractPreferences;
import com.elang.tim.jasatani.helper.HandleVolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailAhliActivity extends AppCompatActivity {

    private String TAG = DetailAhliActivity.class.getName();

    private int id;
    private String title;

    private TextView txtDescription, txtTeam, txtAddress, txtEmail, txtPhone;
    private Button btnContract;

    private ContractPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ahli);

        preferences = new ContractPreferences(getApplicationContext());
        preferences.clearContractPreferences();

        txtDescription = findViewById(R.id.txt_description);
        txtTeam = findViewById(R.id.txt_team);
        txtAddress = findViewById(R.id.txt_address);
        txtEmail = findViewById(R.id.txt_email);
        txtPhone = findViewById(R.id.txt_phone);
        btnContract = findViewById(R.id.btn_contract);

        if (savedInstanceState==null) {
            Bundle extras = getIntent().getExtras();
            if(extras==null) {
                id = -1;
            } else {
                id = extras.getInt("ID");
                Log.d(TAG, String.valueOf(id));
            }
        }

        btnContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailAhliActivity.this, ContractActivity.class);
                startActivity(intent);
            }
        });

        getAhli(id);

        /*Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/
        //getSupportActionBar().setTitle(title);
        //DetailAhliActivity.this.setTitle(title);

        /*CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(title);

        collapsingToolbarLayout.setTitle(title);*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getAhli(final int id) {
        StringRequest request = new StringRequest(Request.Method.POST, AppConfig.URL_DETAIL_AHLI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            Log.d(TAG, "Detail:" + object);
                            int id = object.getInt("id_ahli_tani");
                            String name = object.getString("nama_ahli_tani");
                            String address = object.getString("alamat_ahli_tani");
                            String description = object.getString("deskripsi_ahli_tani");
                            String team = object.getString("tim_tani");
                            String email = object.getString("email_ahli_tani");
                            String phone = object.getString("no_telpon_ahli_tani");

                            Toolbar toolbar = findViewById(R.id.toolbar);
                            setSupportActionBar(toolbar);
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                            getSupportActionBar().setDisplayShowHomeEnabled(true);

                            preferences.setIdAhli(id);
                            DetailAhliActivity.this.setTitle(name);
                            txtAddress.setText(address);
                            if (!description.equals("null")) {
                                txtDescription.setText(description);
                            } else {
                                txtDescription.setText(R.string.not_added);
                            }
                            if (!team.equals("null")) {
                                txtTeam.setText(team);
                            } else {
                                txtTeam.setText(R.string.not_added);
                            }
                            txtEmail.setText(email);
                            txtPhone.setText(phone);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HandleVolleyError handleVolleyError = new HandleVolleyError();
                handleVolleyError.handleVolleyError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_ahli_tani", String.valueOf(id));

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }


}
