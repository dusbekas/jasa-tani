package com.elang.tim.jasatani.model;

/**
 * Created by Arman Muhamad on 25/12/2017.
 */

public class Feedback {

    private int value;
    private String date;
    private String comment;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
