package com.elang.tim.jasatani.presenter;

/**
 * Created by arman on 02/01/2018.
 */

public interface AhliContract {

    interface Presenter {

        void changeProfile(int idAhli);
        void tambahAhliTani();
    }

}
