package com.elang.tim.jasatani.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.activity.LahanDialogueActivity;
import com.elang.tim.jasatani.activity.RegisterActivity;
import com.elang.tim.jasatani.helper.UserPreferences;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    private Button btnLogOut, btnLahan;
    private TextView txtAddress, txtEmail, txtPhone;
    private UserPreferences preferences;

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_pemilik, container, false);
        preferences = new UserPreferences(getActivity().getApplicationContext());

        txtAddress = view.findViewById(R.id.txt_address);
        txtEmail = view.findViewById(R.id.txt_email);
        txtPhone = view.findViewById(R.id.txt_phone);
        btnLogOut = view.findViewById(R.id.btn_log_out);
        btnLahan = view.findViewById(R.id.btn_lahan);
        Toolbar toolbar = view.findViewById(R.id.toolbar);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        /*((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(preferences.getName());
        txtAddress.setText(preferences.getAddress());
        txtEmail.setText(preferences.getEmail());
        txtPhone.setText(preferences.getPhone());

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.clearAhliTaniPreferences();
                FirebaseAuth.getInstance().signOut();

                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btnLahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), LahanDialogueActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

}
