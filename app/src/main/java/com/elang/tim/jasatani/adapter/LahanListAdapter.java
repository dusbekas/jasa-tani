package com.elang.tim.jasatani.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elang.tim.jasatani.R;
import com.elang.tim.jasatani.model.Lahan;
import com.elang.tim.jasatani.presenter.LahanContract;
import com.elang.tim.jasatani.presenter.LahanPresenter;

import java.util.List;

/**
 * Created by arman on 06/01/2018.
 */

public class LahanListAdapter extends RecyclerView.Adapter<LahanListAdapter.ViewHolder> {

    private LahanContract.Presenter presenter;
    private LahanContract.View view;
    private List<Lahan> lahanList;
    private Context context;
    private Activity activity;

    public LahanListAdapter(
            List<Lahan> lahanList,
            Context context,
            LahanContract.View view,
            Activity activity) {
        this.lahanList = lahanList;
        this.context = context;
        this.view = view;
        this.activity = activity;
    }

    @Override
    public LahanListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_browse_lahan, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Lahan lahan = lahanList.get(position);
        final String title;
        String location = lahan.getLocation();
        if (lahan.getArea()<100) {
            title = lahan.getDescription() + " " + lahan.getArea() + "m";
        } else {
            title = lahan.getDescription() + " " + lahan.getArea()/100 + "ha";
        }

        holder.title.setText(title);
        holder.location.setText(location);

        presenter = new LahanPresenter(this.view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onRecyclerClicked("Test", context, lahanList.get(position).getId());
                Intent intent = new Intent();
                intent.putExtra("LAHAN", title);
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return lahanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView location;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_browse_lahan_judul);
            location = itemView.findViewById(R.id.txt_browse_lahan_lokasi);
        }
    }

}
