package com.elang.tim.jasatani.model;

/**
 * Created by Arman Muhamad on 25/12/2017.
 */

public class Kontrak {
    private int idAhli;
    private int idLahan;
    private String date;
    private int duration;
    private String status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getIdAhli() {
        return idAhli;
    }

    public void setIdAhli(int idAhli) {
        this.idAhli = idAhli;
    }

    public int getIdLahan() {
        return idLahan;
    }

    public void setIdLahan(int idLahan) {
        this.idLahan = idLahan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
