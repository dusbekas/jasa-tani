package com.elang.tim.jasatani.presenter;

import android.content.Context;

import com.elang.tim.jasatani.model.Kontrak;

import java.util.List;

/**
 * Created by arman on 02/01/2018.
 */

public interface KontrakContract {

    interface Presenter{

        void createContract(int idAhli, int idLahan, String date, String duration);
        void createDate(int year, int monthOfYear, int dayOfMonth);
        void createLahan(String lahan);
        void getKontrakList(int idPemilik, String status);
    }

    interface View {

        void updateLahan(String lahan);
        void updateDate(String date);
        void finishSendContract(String message);
        void showKontrakList(List<Kontrak> kontrakList);

    }
}
