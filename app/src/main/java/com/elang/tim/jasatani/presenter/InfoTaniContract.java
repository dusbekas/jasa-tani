package com.elang.tim.jasatani.presenter;

/**
 * Created by arman on 02/01/2018.
 */

public interface InfoTaniContract {

    interface Presenter {

        void addInfo();

        void deleteInfo();

        void editInfo();

    }
}
