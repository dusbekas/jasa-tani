package com.elang.tim.jasatani.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by arman on 03/07/2017.
 */

public class UserPreferences {
    // LogCat tag
    private static String TAG = UserPreferences.class.getName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "UserPreferences";

    private String ID = "id";
    private String NAME = "name";
    private String EMAIL = "email";
    private String PHONE = "phone";
    private String ADDRESS = "address";
    private String UID = "uid";
    private String ACC_TYPE = "acc_type";

    public UserPreferences(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setId(int data) {
        editor.putInt(ID, data);
        editor.commit();
        Log.d(TAG, "id has been modified!");
    }

    public void setName(String data) {
        editor.putString(NAME, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "Name has been modified!");
    }

    public void setEmail(String data) {
        editor.putString(EMAIL, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "Email has been modified!");
    }

    public void setPhone(String data) {
        editor.putString(PHONE, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "Phone has been modified!");
    }

    public void setAddress(String data) {
        editor.putString(ADDRESS, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "Address has been modified!");
    }

    public void setUid(String data) {
        editor.putString(UID, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "UID has been modified!");
    }

    public void setAccType(String data) {
        editor.putString(ACC_TYPE, data);
        // commit changes
        editor.commit();
        Log.d(TAG, "Account type has been modified!");
    }

    public int getId() {
        return pref.getInt(ID, -1);
    }

    public String getName() {
        return pref.getString(NAME, "");
    }

    public String getEmail() {
        return pref.getString(EMAIL, "");
    }

    public String getPhone() {
        return pref.getString(PHONE, "");
    }

    public String getAddress() {
        return pref.getString(ADDRESS, "");
    }

    public String getUid() {
        return pref.getString(UID, "");
    }

    public String getAccType() {
        return pref.getString(ACC_TYPE, "");
    }

    public void clearAhliTaniPreferences(){
        editor.clear();
        editor.apply();
    }
}
